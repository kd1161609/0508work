//  5/7ソフトウェア開発実験 実習基礎データ
import java.util.Scanner;
public class CheckNum {
    public static void main(String[] args) {
        Integer num;

        num = getNum(); // プロンプトを出して数字を入力させる
        System.out.println("偶数ですか？ >" + conv(isEven(num)));
        System.out.println("素数ですか？ >" + conv(isPrime(num)));
    }


    public static String conv(Boolean f) {
	if(f){
		return "はい";
	} else {
		return "いいえ";
	}
    }

    // 適当なプロンプトを出力し、数値を入力させる(その数値を返す)
    public static Integer getNum() {
        Scanner scan = new Scanner(System.in);
	System.out.println("数字を入力してください。");
	int number;
	number=scan.nextInt();
	return number;
    }

    // 渡された値が偶数かをチェックする(戻り値はBoolean)
    public static Boolean isEven(Integer n) {
	if(n%2==0){
		return true;	//偶数の場合true返す
	
	}else{
		return false;
	}
    }

// 渡された値が素数かをチェックする(戻り値はBoolean)
    public static Boolean isPrime(Integer n) {
	Boolean f=true;
	if(n<=1){
		f=false;
	}else{
		for(int i=2;i<n;i++){
			if(n%i==0){
				f=false;
				break;
			}
		}
	}
	return f;
    }
}
